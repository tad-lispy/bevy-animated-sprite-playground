{
  description = "Small playground for sprite animation in Bevy";

  inputs = {
    nixpkgs.url      = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    flake-utils.url  = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, rust-overlay, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        project-name = "bevy-animated-sprite-playground";
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        rust = pkgs.rust-bin.stable.latest.minimal.override {
          targets = [ "wasm32-unknown-unknown" ];
        };
        rustPlatform = pkgs.makeRustPlatform {
          cargo = rust;
          rustc = rust;
        };
        buildInputs = with pkgs; [
          openssl
        ];
        nativeBuildInputs = with pkgs; [
          rust
          gcc
          pkg-config
          gnumake
          wasm-bindgen-cli
          trunk
          rustPlatform.bindgenHook
          binaryen
          brotli
          findutils
          gzip
        ];
      in
      {
        devShell = pkgs.mkShell {
          inherit buildInputs nativeBuildInputs;
          name = "${project-name}-develpoment-shell";
          packages = with pkgs; [
            (rust-bin.fromRustupToolchainFile ./rust-toolchain.toml)
            pkgs.rust-analyzer
            pkgs.miniserve
          ];
        };
        defaultPackage = rustPlatform.buildRustPackage {
          name = project-name;
          version = "1.0.0";
          src = ./.;
          cargoLock = {
            lockFile = ./Cargo.lock;
          };
          buildPhase = "make";
          checkPhase = "make test";
          installPhase = "make install";

          inherit buildInputs nativeBuildInputs;
        };
        packages.docker-image = pkgs.dockerTools.buildLayeredImage {
          name = "${project-name}-build-image";
          tag = "latest";
          created = "now";
          contents = buildInputs ++ nativeBuildInputs ++ [
            pkgs.bash
            pkgs.coreutils
            pkgs.git
            pkgs.httpie
            pkgs.rustup
            pkgs.cacert
          ];
          fakeRootCommands = ''
            mkdir --parents /tmp
          '';
          enableFakechroot = true;
          config.Cmd = [ "bash" ];
        };
      }
    );
}
