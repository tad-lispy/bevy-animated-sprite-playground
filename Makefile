name := bevy-animated-sprite-playground

include Makefile.d/defaults.mk

all: ## Build the program (DEFAULT)
all: dist
.PHONY: all

dist: $(shell find src/)
dist: $(shell find web/)
dist: Cargo.lock
dist: index.html
dist: base_url ?= "/"
dist:
	trunk build \
		--release \
		--public-url $(base_url) \
		--dist $@
	touch $@

test:
	@echo "Not implemented"
.PHONY: test

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: dist
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive $</* $(prefix)/
.PHONY: install

public: ## Prepare the web assets (used in CI)
public: dist
public:
	rm --recursive --force $@
	cp --recursive --force $^ $@
	unset GZIP
	find $@ -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\|wasm\)$$' -exec gzip -9 -f -k {} \;
	find $@ -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\|wasm\)$$' -exec brotli --force --keep {} \;
	touch $@
.PHONY: public


### DEVELOPMENT

develop: ## Rebuild and run a development version of the program
develop:
	trunk serve \
		--address=0.0.0.0 \
		index.html
.PHONY: develop


serve: ## Serve a release version of the program
serve: dist
serve:
	miniserve --index=index.html $<
.PHONY: serve

clean: ## Remove all build artifacts
clean:
	git clean -dfX \
		--exclude='!.envrc.private'
.PHONY: clean

help: ## Print this help message
help:
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help

### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)

serve-result: ## Build with Nix and serve the result. The ultimate test.
serve-result: result
serve-result:
	miniserve --index=index.html result/
.PHONY: serve-result

### OCI BUILD IMAGE RELATED

build-image: ## Create an OCI image with build environment (to be used in CI)
build-image: flake.nix
build-image: flake.lock
build-image: rust-toolchain.toml
build-image:
	nix build \
		--print-build-logs \
		.#docker-image
	cp --dereference result $@

load-build-image: ## Load the OCI image using Podman
load-build-image: build-image
	podman image load --input $^
.PHONY: load-build-image

build-inside-container: ## Build the program in an OCI container (e.g. to test CI)
build-inside-container: load-build-image
	podman run \
		--rm \
		--interactive \
		--tty \
		--volume ${PWD}:/src \
		--workdir /src \
		--entrypoint make \
		localhost/bevy-animated-sprite-playground-build-image \
		public
.PHONY: build-inside-container
