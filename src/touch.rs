use crate::{Systems, Configuration};
use bevy::prelude::*;
use wasm_bindgen::prelude::*;

pub struct TouchPlugin;

impl Plugin for TouchPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<TouchPoints>()
            .add_startup_system(setup_touch_events)
            .add_system_set(
                SystemSet::new()
                    .with_system(touch.label(SubSystems::Touch))
                    .with_system(mouse.after(SubSystems::Touch).label(SubSystems::Mouse))
                    .label(Systems::Input),
            );
    }
}

#[derive(SystemLabel, Debug, Clone, PartialEq, Eq, Hash)]
enum SubSystems {
    Touch,
    Mouse,
}

// TOUCH INPUT

#[derive(Default, Debug)]
pub struct TouchPoints(pub Vec<TouchPoint>);

impl TouchPoints {
    pub fn get(&self, identifier: i64) -> Option<&TouchPoint> {
        self.0.iter().find(|touch| touch.identifier == identifier)
    }
}

#[derive(Debug, Clone, Copy)]
pub struct TouchPoint {
    pub translation: Vec3,
    pub identifier: i64,
}

impl TouchPoint {
    fn from_js(scale: f32, js_touch: JsTouchPoint, window: &Window) -> Self {
        let x = (js_touch.x as f32 - window.width() / 2.) / scale;
        let y = (js_touch.y as f32 - window.height() / 2.) / -scale;

        Self {
            identifier: js_touch.identifier,
            translation: Vec3 { x, y, z: 0.0 },
        }
    }
}

// /// Idiomatic Bevy touch input system.
// FIXME: Use this once https://github.com/bevyengine/bevy/issues/4565 is solved
//
// fn touch(touch: Res<TouchPoints>, mut zombies: Query<&mut Activity>) {
//     let count = touch.iter().count();
//     if count > 0 {
//         for activity in zombies.iter_mut() {
//             launch_attack(activity);
//         }
//     }

//     debug!("Fingers touching: {count}");
//     for finger in touch.iter() {
//         println!(
//             "Finger {} is at position ({},{}), started from ({},{}).",
//             finger.id(),
//             finger.position().x,
//             finger.position().y,
//             finger.start_position().x,
//             finger.start_position().y,
//         );
//     }
// }

/// Alternative implementation of touch input system that relies on JS interop
fn touch(mut touches: ResMut<TouchPoints>, windows: Res<Windows>, configuration: Res<Configuration>) {
    let window = windows.primary();
    touches.0 = get_touch_points()
        .iter()
        .map(JsTouchPoint::from)
        .map(|jstouch| TouchPoint::from_js(configuration.scale, jstouch, window))
        .collect();
}

// JS INTEROP

/// This is a raw JS touch point.
///
/// It is extracted from a [JS Touch] value. Most notably it's coordinate system
/// is different than the one used in internal `TouchPoint` struct:
///
/// 1. The origin (0, 0) is in the top left corner of the window.
///
/// 2. The Y axis is downward.
///
/// [JS Touch]: https://developer.mozilla.org/en-US/docs/Web/API/Touch
#[derive(Debug)]
struct JsTouchPoint {
    x: f64,
    y: f64,
    identifier: i64,
}

impl From<&JsValue> for JsTouchPoint {
    fn from(value: &JsValue) -> Self {
        let x_key = js_sys::JsString::from("x");
        let y_key = js_sys::JsString::from("y");
        let identifier_key = js_sys::JsString::from("identifier");

        let x = js_sys::Reflect::get(value, &x_key)
            .unwrap()
            .as_f64()
            .unwrap();
        let y = js_sys::Reflect::get(value, &y_key)
            .unwrap()
            .as_f64()
            .unwrap();
        let identifier = js_sys::Reflect::get(value, &identifier_key)
            .unwrap()
            .as_f64()
            .unwrap() as i64;
        // let identifier = "js_sys::Reflect::get(&value, &identifier_key).unwrap().as_string().unwrap()".to_string();
        Self { x, y, identifier }
    }
}

// These are defined in the index.html file
// TODO: Use wasm-bindgen JS snippets? See https://trunkrs.dev/assets/#js-snippets
#[wasm_bindgen]
extern "C" {
    fn setup_touch_events();
    fn get_touch_points() -> Vec<JsValue>;
}

// MOUSE

fn mouse(
    buttons: Res<Input<MouseButton>>,
    windows: Res<Windows>,
    mut touches: ResMut<TouchPoints>,
    configuration: Res<Configuration>
) {
    if touches.0.clone().into_iter().filter(|point| point.identifier != -1).count() > 0 {
        return
    };

    if buttons.pressed(MouseButton::Left) {
        let window = windows.primary();
        if let Some(position) = window.cursor_position() {
            let size = Vec2::new(window.width(), window.height());
            let position = position - size / 2.0;
            let translation = position.extend(0.0) / configuration.scale;
            touches.0.push(TouchPoint {
                translation,
                identifier: -1,
            })
        }
    }
}
