use crate::touch::TouchPoints;
use crate::{is_debug, Configuration};
use bevy::prelude::*;
use bevy::sprite::{MaterialMesh2dBundle, Mesh2dHandle};
use bevy_rapier2d::prelude::Collider;

pub const SIZE: f32 = 30.0;

pub struct FingersPlugin;

impl Plugin for FingersPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup_shape)
            .add_system(spawn_fingers)
            .add_system(update_fingers)
            .add_system(grow_fingers)
            .add_system(shrink_fingers);
    }
}

#[derive(Component, Debug)]
pub struct Finger;

#[derive(Component, Debug, Deref)]
struct Size(f32);

/// Component to connect entity with a touch point
///
/// Useful for spawning and removing entities.
#[derive(Component, Debug, PartialEq, Eq)]
struct TouchId(i64);

impl Size {
    fn grow(&mut self, delta: f32) -> &mut Self {
        const RATE: f32 = 4.0;

        self.0 = (self.0 + RATE * delta).clamp(0., 1.);
        self
    }

    fn shrink(&mut self, delta: f32) -> &mut Self {
        const RATE: f32 = 1.0;

        self.0 = (self.0 - RATE * delta).clamp(0., 1.);
        self
    }
}

struct FingerShape {
    mesh: Mesh2dHandle,
    material: Handle<ColorMaterial>,
}

fn setup_shape(
    mut shapes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut commands: Commands,
) {
    let shape = FingerShape {
        mesh: shapes.add(shape::Circle::new(SIZE).into()).into(),
        material: materials.add(Color::CRIMSON.into()),
    };
    commands.insert_resource(shape)
}

fn spawn_fingers(
    touches: Res<TouchPoints>,
    fingers: Query<&TouchId, With<Finger>>,
    shape: Res<FingerShape>,
    mut commands: Commands,
) {
    for touch in touches.0.iter() {
        if !fingers.iter().any(|finger| finger.0 == touch.identifier) {
            debug!("New touch: {identifier}", identifier = touch.identifier);
            commands
                .spawn_bundle(MaterialMesh2dBundle {
                    mesh: shape.mesh.to_owned(),
                    material: shape.material.to_owned(),
                    transform: Transform::from_translation(touch.translation)
                        .with_scale(Vec3::splat(0.)),
                    visibility: Visibility {
                        is_visible: is_debug() || touch.identifier == -1,
                    },
                    ..default()
                })
                .insert(Finger)
                .insert(Size(0.))
                .insert(Collider::ball(SIZE))
                .insert(TouchId(touch.identifier));
        }
    }
}

fn update_fingers(
    touches: Res<TouchPoints>,
    mut fingers: Query<(Entity, &mut Transform, &mut TouchId), With<Finger>>,
    mut commands: Commands,
) {
    for (finger, mut transform, id) in fingers.iter_mut() {
        let TouchId(identifier) = *id;
        if let Some(touch) = touches.get(identifier) {
            transform.translation = touch.translation;
        } else {
            debug!("Touch {identifier} removed");
            commands.entity(finger).remove::<TouchId>();
        }
    }
}

fn grow_fingers(
    mut fingers: Query<(&mut Size, &mut Transform), (With<Finger>, With<TouchId>)>,
    time: Res<Time>,
    configuration: Res<Configuration>
) {
    for (mut size, mut transform) in fingers.iter_mut() {
        size.grow(time.delta_seconds());
        transform.scale = Vec3::splat(size.0 / configuration.scale);
    }
}

fn shrink_fingers(
    mut fingers: Query<(Entity, &mut Size, &mut Transform), (With<Finger>, Without<TouchId>)>,
    mut commands: Commands,
    time: Res<Time>,
    configuration: Res<Configuration>
) {
    for (finger, mut size, mut transform) in fingers.iter_mut() {
        size.shrink(time.delta_seconds());
        if size.0 > 0. {
            transform.scale = Vec3::splat(size.0 / configuration.scale);
        } else {
            debug!("Despawning finger");
            commands.entity(finger).despawn_recursive();
        }
    }
}
