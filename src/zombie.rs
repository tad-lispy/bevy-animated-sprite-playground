use std::{
    f32::{consts::PI, INFINITY},
    ops::{Mul, Sub},
};

use crate::{
    animation,
    animation::{Animation, AnimationFramesSpec},
    directives::{Directive, Directives},
    fingers::{self as fingers, Finger},
    AppState, Configuration, Systems,
};
use bevy::{
    asset::{HandleId, LoadState},
    prelude::*,
    sprite::Anchor, render::primitives::Aabb,
};
use bevy_rapier2d::prelude::*;
use enum_iterator::Sequence;

pub struct ZombiePlugin;

impl Plugin for ZombiePlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_enter(AppState::Loading).with_system(Animations::initialize),
        )
        .add_system_set(SystemSet::on_update(AppState::Loading).with_system(check_textures))
        .add_system_set(SystemSet::on_enter(AppState::Ready).with_system(spawn_zombies))
        .add_system_set(
            SystemSet::on_update(AppState::Ready)
                .with_system(set_activity.after(Systems::Input))
                .with_system(set_animation.after(set_activity))
                .with_system(animate.after(set_animation))
                .with_system(motion.after(set_animation)),
        );
    }
}

#[derive(Component, Debug)]
pub struct Zombie;

// ACTIVITIES

/// An activity describes what a zombie is doing
///
/// A zombie can only be performing a single activity at a time.
#[derive(Component, Debug, PartialEq, Eq, Hash, Sequence, Clone, Copy)]
pub enum Activity {
    None,
    Walking(Leg),
    Reversing(Leg),
    Turning(Turn),
    Attacking,
}

impl Activity {
    fn active_leg(&self) -> Leg {
        match self {
            Activity::None => Leg::default(),
            Activity::Walking(leg) => *leg,
            Activity::Reversing(leg) => *leg,
            Activity::Turning(_) => Leg::default(),
            Activity::Attacking => Leg::default(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Sequence, Clone, Copy)]
pub enum Leg {
    Left,
    Right,
}

impl Default for Leg {
    fn default() -> Self {
        Self::Left
    }
}

impl Leg {
    fn opposite(&self) -> Self {
        match self {
            Self::Left => Self::Right,
            Self::Right => Self::Left,
        }
    }
}

impl From<Activity> for animation::AnimationFramesSpec {
    fn from(val: Activity) -> Self {
        match val {
            Activity::None => AnimationFramesSpec {
                entity: "zombie".into(),
                activity: "idle".into(),
                total_frames: 17,
                first_frame: 0,
                length: 17,
                reverse: false,
            },

            Activity::Walking(Leg::Left) => AnimationFramesSpec {
                entity: "zombie".into(),
                activity: "walk".into(),
                total_frames: 17,
                first_frame: 9,
                length: 8,
                reverse: false,
            },
            Activity::Walking(Leg::Right) => AnimationFramesSpec {
                entity: "zombie".into(),
                activity: "walk".into(),
                total_frames: 17,
                first_frame: 0,
                length: 9,
                reverse: false,
            },

            Activity::Reversing(Leg::Left) => AnimationFramesSpec {
                entity: "zombie".into(),
                activity: "walk".into(),
                total_frames: 17,
                first_frame: 9,
                length: 8,
                reverse: true,
            },
            Activity::Reversing(Leg::Right) => AnimationFramesSpec {
                entity: "zombie".into(),
                activity: "walk".into(),
                total_frames: 17,
                first_frame: 0,
                length: 9,
                reverse: true,
            },

            Activity::Turning(Turn::Left) => AnimationFramesSpec {
                entity: "zombie".into(),
                activity: "walk".into(),
                total_frames: 17,
                first_frame: 1,
                length: 10,
                reverse: false,
            },
            Activity::Turning(Turn::Right) => AnimationFramesSpec {
                entity: "zombie".into(),
                activity: "walk".into(),
                total_frames: 17,
                first_frame: 9,
                length: 10,
                reverse: false,
            },
            Activity::Attacking => AnimationFramesSpec {
                entity: "zombie".into(),
                activity: "attack".into(),
                total_frames: 9,
                first_frame: 0,
                length: 9,
                reverse: false,
            },
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Sequence, Clone, Copy)]
pub enum Turn {
    Left,
    Right,
}

impl std::fmt::Display for Turn {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Turn::Left => f.write_str("left"),
            Turn::Right => f.write_str("right"),
        }
    }
}

pub fn launch_attack(activity: &Activity) -> Activity {
    debug!("Launch order received. Current activity is {activity:?}.");
    match *activity {
        Activity::None => Activity::Attacking,
        Activity::Walking(_) => Activity::Attacking,
        Activity::Reversing(_) => Activity::Attacking,
        Activity::Turning(_) => Activity::Attacking,
        Activity::Attacking => {
            debug!("Can't launch a new attack when already attacking.");
            *activity
        }
    }
}

pub fn step_forward(activity: &Activity, leg: Leg) -> Activity {
    debug!("Walk order received. Current activity is {activity:?}.");
    match *activity {
        Activity::None => Activity::Walking(leg),
        Activity::Turning(_) => Activity::Walking(leg),
        Activity::Walking(_) => {
            debug!("Already walking.");
            *activity
        }
        Activity::Reversing(_) => {
            debug!("Can't step forward while reversing.");
            *activity
        }
        Activity::Attacking => {
            debug!("Can't step forward while attacking.");
            *activity
        }
    }
}

pub fn step_back(activity: &Activity, leg: Leg) -> Activity {
    debug!("Reverse order received. Current activity is {activity:?}.");
    match *activity {
        Activity::None => Activity::Reversing(leg),
        Activity::Turning(_) => Activity::Reversing(leg),
        Activity::Walking(_) => {
            debug!("Can't reverse while walking forward.");
            *activity
        }
        Activity::Reversing(_) => {
            debug!("Already reversing.");
            *activity
        }
        Activity::Attacking => {
            debug!("Can't step forward while attacking.");
            *activity
        }
    }
}

pub fn pivot(activity: &Activity, turn: Turn) -> Activity {
    debug!("About to turn {turn}. Current activity is {activity:?}.");
    match *activity {
        Activity::None => Activity::Turning(turn),
        Activity::Walking(_) => {
            debug!("Can't turn while stepping forward.");
            *activity
        }
        Activity::Reversing(_) => {
            debug!("Can't turn while stepping back.");
            *activity
        }
        Activity::Turning(_) => {
            debug!("Already turning.");
            *activity
        }
        Activity::Attacking => {
            debug!("Can't launch a new attack when already attacking.");
            *activity
        }
    }
}

// SPRITES AND ANIMATION
type Animations = animation::Library<Activity>;

fn check_textures(
    mut state: ResMut<State<AppState>>,
    animations: Res<Animations>,
    assets_server: Res<AssetServer>,
) {
    let handles_ids = animations
        .all_frames()
        .iter()
        .map(|handle| handle.id)
        .collect::<Vec<HandleId>>();
    if let LoadState::Loaded = assets_server.get_group_load_state(handles_ids) {
        info!("Loaded");
        state.set(AppState::Ready).expect("Change state");
    }
}

fn spawn_zombies(
    configuration: Res<Configuration>,
    mut commands: Commands,
    animations: Res<Animations>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    mut textures: ResMut<Assets<Image>>,
) {
    let mut texture_atlas_builder = TextureAtlasBuilder::default().initial_size(Vec2::splat(256.));

    for handle in animations.all_frames() {
        let texture = textures.get(&handle).expect("Failed to get the texture.");
        texture_atlas_builder.add_texture(handle.to_owned(), texture);
    }
    let texture_atlas = texture_atlas_builder.finish(&mut textures).unwrap();

    let columns: usize = (configuration.zombies as f32).sqrt().floor() as usize;
    // TODO: Use quantity.div_ceil(columns) once int_roundings feature is stable
    // see https://github.com/rust-lang/rust/issues/88581)
    let rows: usize = (configuration.zombies + columns - 1) / columns;

    for index in 0..configuration.zombies {
        let center = 0.5
            * Vec2 {
                x: rows as f32,
                y: columns as f32,
            };

        let row = index / columns;
        let column = index % columns;

        let translation = Vec2::new(row as f32, column as f32)
            .sub(center)
            .mul(configuration.spread)
            .extend(0.0);

        let handle = texture_atlases.add(texture_atlas.clone());

        let activity = Activity::None;
        let animation = Animation::from(
            animations
                .get(&activity)
                .expect("No frames for {activity:?} animation"),
        );

        commands
            .spawn_bundle(SpriteSheetBundle {
                texture_atlas: handle,
                sprite: TextureAtlasSprite {
                    anchor: Anchor::Custom(Vec2 { x: -0.2, y: 0.0 }),
                    ..Default::default()
                },
                transform: Transform::from_translation(translation),
                ..Default::default()
            })
            .insert(Zombie)
            .insert(RigidBody::Dynamic)
            .insert(Collider::ball(100.))
            .insert(ColliderMassProperties::Mass(120.0))
            .insert(Damping {
                linear_damping: 20.0,
                angular_damping: 20.0,
            })
            .insert(ExternalForce::default())
            .insert(activity)
            .insert(animation);
    }
}
/// This system is responsible for switching from one animation to another when the activity changes
fn set_animation(
    mut zombies: Query<(&mut Animation, &Activity), Changed<Activity>>,
    animations: Res<Animations>,
) {
    for (mut animation, activity) in zombies.iter_mut() {
        debug!("Starting animation for {activity:?}.");
        *animation = animations
            .get(activity)
            .expect(&format!("No frames for {activity:?} found"))
            .into();
    }
}

/// This system is responsible for progressing the animation
fn animate(
    time: Res<Time>,
    mut entities: Query<(
        &mut Animation,
        &mut TextureAtlasSprite,
        &Handle<TextureAtlas>,
    )>,
    atlases: Res<Assets<TextureAtlas>>,
) {
    for (mut animation, mut texture, atlas_handle) in entities.iter_mut() {
        animation.update(time.delta());
        let atlas = atlases.get(atlas_handle).unwrap();
        let frame = animation.frame();

        let sprite_index = atlas
            .get_texture_index(&frame)
            .expect("The handle is not in the atlas");
        texture.index = sprite_index;
    }
}

fn motion(mut zombies: Query<(&mut ExternalForce, &Transform, &Activity, &Animation)>) {
    for (mut force, transform, activity, animation) in zombies.iter_mut() {
        match activity {
            Activity::None => {
                force.force = Vec2::ZERO;
                force.torque = 0.0;
            }
            Activity::Walking(_) => {
                force.force = transform.local_x().truncate() * 1_000_000.0;
                force.torque = 0.0;
            }
            Activity::Reversing(_) => {
                force.force = transform.local_x().truncate() * -750_000.0;
                force.torque = 0.0;
            }
            Activity::Turning(turn) => {
                let frame = animation.state.frame_index();
                if frame < 4 {
                    force.force = transform.local_x().truncate() * 1_000_000.0;
                    force.torque = match turn {
                        Turn::Left => 1_000.,
                        Turn::Right => -1_000.,
                    };
                } else {
                    force.force = Vec2::ZERO;
                    force.torque = 0.0;
                }
            }
            Activity::Attacking => {
                force.force = Vec2::ZERO;
                force.torque = 0.0;
            }
        }
    }
}

/// This system changes the activity based on directives and animation state
fn set_activity(
    mut zombies: Query<(&mut Activity, &Animation, &Transform)>,
    other_zombies: Query<&Transform, With<Zombie>>,
    rapier: Res<RapierContext>,
    fingers: Query<&Transform, With<Finger>>,
    directives: Res<Directives>,
    configuration: Res<Configuration>
) {
    // Are we turning left or right?
    let mut turn = 0;
    if directives.contains(&Directive::TurnLeft) {
        turn -= 1
    };
    if directives.contains(&Directive::TurnRight) {
        turn += 1
    };

    // Are we moving forward or reversing?
    let mut speed = 0;
    if directives.contains(&Directive::Walk) {
        speed += 1
    };
    if directives.contains(&Directive::Reverse) {
        speed -= 1
    };

    let mut index = 0;
    for (mut activity, animation, transform) in zombies.iter_mut() {
        index += 1;

        let current_activity = *activity;

        if animation.state.is_ended() {
            debug!("Zombie {index} is done {current_activity:?}.");
            *activity = Activity::None
        }

        let new_activity = if directives.contains(&Directive::Attack) {
            launch_attack(&activity)
        } else if speed == 1 {
            step_forward(&activity, current_activity.active_leg().opposite())
        } else if speed == -1 {
            step_back(&activity, current_activity.active_leg().opposite())
        } else if turn == -1 {
            pivot(&activity, Turn::Left)
        } else if turn == 1 {
            pivot(&activity, Turn::Right)
        } else if let Some(finger) = nearest(transform, fingers.iter()) {
            let target = finger.translation.truncate();
            let origin = transform.translation.truncate();
            let path = target - origin;
            let direction = path.angle_between(transform.local_x().truncate());
            let distance = origin.distance(target);

            let is_looking_at = direction.abs() < 0.1;
            let is_behind = direction.abs() > PI / 2.0;
            let is_within_reach = is_looking_at && distance < 180.0 + fingers::SIZE / configuration.scale;
            let is_too_close = distance < 110.0 + fingers::SIZE / configuration.scale;

            if is_too_close {
                if is_behind {
                    step_forward(&activity, current_activity.active_leg().opposite())
                } else {
                    step_back(&activity, current_activity.active_leg().opposite())
                }
            } else if is_within_reach {
                launch_attack(&activity)
            } else if is_looking_at {
                step_forward(&activity, current_activity.active_leg().opposite())
            } else {
                let turn = if direction < 0.0 {
                    Turn::Left
                } else {
                    Turn::Right
                };
                pivot(&activity, turn)
            }
        } else if *activity == Activity::None {
            let size = 300.0;
            let diagonal = Vec2::splat(size).extend(0.0);
            let min = transform.translation - diagonal;
            let max = transform.translation + diagonal;
            let aabb = Aabb::from_min_max(min, max);

            let mut count = 0;
            let mut path = Vec2::ZERO;
            rapier.colliders_with_aabb_intersecting_aabb(aabb, |entity| {
                // If there is more than 3 neighbors, stop.
                // This is a performance optimization. In a dense crowd it
                // probably doesn't matter which direction we go.
                if count > 3 { return false }
                count += 1;

                if let Ok(other) = other_zombies.get(entity) {
                    let target = other.translation.truncate();
                    let origin = transform.translation.truncate();

                    // Don't take yourself into account.
                    if target == origin { return true };

                    // Prefer direction away from the neighbors.
                    //
                    // Please note that this is very crude. For example,
                    // a neighbor further away will have stronger influence than
                    // the one close by. This is probably the opposite of what
                    // would actually happen. We could divide the resulting
                    // vector by distance (or distance squared). But since the
                    // point is to spread the crowd, it doesn't matter so much
                    // how individual behave. Besides, these are zombies, so we
                    // shouldn't expect much rational behavior :-)
                    path += target - origin;
                };
                true
            });
            if path == Vec2::ZERO { continue };
            let direction = path.angle_between(transform.local_x().truncate());

            let is_looking_at = direction.abs() < 0.5;
            let is_behind = direction.abs() > PI / 1.5;

            if is_looking_at {
                step_back(&activity, current_activity.active_leg().opposite())
            } else if is_behind {
                step_forward(&activity, current_activity.active_leg().opposite())
            } else {
                let turn = if direction < 0.0 {
                    Turn::Right
                } else {
                    Turn::Left
                };
                pivot(&activity, turn)
            }
        } else { continue; };

        if *activity != new_activity {
            debug!("Zombie {index} is switching from {current_activity:?} to {new_activity:?}.");
            *activity = new_activity;
        };
    }
}

fn nearest<'a, I>(origin: &Transform, candidates: I) -> Option<Transform>
where
    I: IntoIterator<Item = &'a Transform>,
{
    let mut best_candidate: Option<Transform> = None;
    let mut shortest_distance = INFINITY;

    for candidate in candidates.into_iter() {
        // It might happen that the origin is among the candidate (e.g. when looking for a nearest zombie)
        // In that case ignore it and keep looking for nearest other.
        if candidate.translation == origin.translation {
            continue;
        }

        let distance = candidate.translation.distance(origin.translation);
        if best_candidate == None || distance < shortest_distance {
            best_candidate = Some(*candidate);
            shortest_distance = distance;
        }
    }

    best_candidate
}
