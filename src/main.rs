mod animation;
mod diagnostics;
mod directives;
mod fingers;
mod keyboard;
mod touch;
mod zombie;

use bevy::{log::LogSettings, prelude::*};
use bevy_rapier2d::prelude::*;
use diagnostics::DiagnosticsPlugin;
use directives::DirectivesPlugin;
use fingers::FingersPlugin;
use keyboard::KeyboardPlugin;
use touch::TouchPlugin;
use wasm_bindgen::prelude::*;
use zombie::ZombiePlugin;

fn main() {
    App::new()
        .insert_resource(Configuration::get())
        .insert_resource(WindowDescriptor {
            fit_canvas_to_parent: true,
            ..Default::default()
        })
        .insert_resource(LogSettings {
            level: if is_debug() {
                bevy::log::Level::DEBUG
            } else {
                bevy::log::Level::INFO
            },
            ..default()
        })
        .insert_resource(RapierConfiguration {
            gravity: Vec2::ZERO,
            ..Default::default()
        })
        .add_state(AppState::Loading)
        .add_plugins(DefaultPlugins)
        .add_plugin(DiagnosticsPlugin)
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(300.0))
        .add_plugin(DirectivesPlugin)
        .add_plugin(FingersPlugin)
        .add_plugin(KeyboardPlugin)
        .add_plugin(TouchPlugin)
        .add_plugin(ZombiePlugin)
        .add_system_set(SystemSet::on_enter(AppState::Ready).with_system(setup_camera))
        .run()
}

struct Configuration {
    scale: f32,
    zombies: usize,
    spread: f32,
}

impl Default for Configuration {
    fn default() -> Self {
        Self {
            scale: 0.5,
            zombies: 20,
            spread: 150.0,
        }
    }
}

// TODO: Make less repetitive
impl Configuration {
    fn get() -> Configuration {
        Self {
            scale: get_query_param("scale")
                .map(|string| string.parse().unwrap())
                .unwrap_or(Configuration::default().scale),
            zombies: get_query_param("zombies")
                .map(|string| string.parse().unwrap())
                .unwrap_or(Configuration::default().zombies),
            spread: get_query_param("spread")
                .map(|string| string.parse().unwrap())
                .unwrap_or(Configuration::default().spread),
        }
    }
}

#[derive(SystemLabel, Debug, Clone, PartialEq, Eq, Hash)]
enum Systems {
    DirectivesReset,
    Input,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum AppState {
    Loading,
    Ready,
}

fn setup_camera(mut commands: Commands, configuration: Res<Configuration>) {
    let mut camera = Camera2dBundle::default();
    camera.projection.scale = 1.0 / configuration.scale;
    commands.spawn_bundle(camera);
}

// JS INTEROP

#[wasm_bindgen]
extern "C" {
    fn is_debug() -> bool;
    fn get_query_param(key: &str) -> Option<String>;
}
