use crate::Systems;
use bevy::{prelude::*, utils::HashSet};

pub struct DirectivesPlugin;

impl Plugin for DirectivesPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<Directives>()
            .add_system(reset.label(Systems::DirectivesReset));
    }
}

#[derive(PartialEq, Eq, Hash, Debug)]
pub enum Directive {
    Walk,
    Reverse,
    TurnLeft,
    TurnRight,
    Attack,
}

#[derive(Deref, DerefMut, Default)]
pub struct Directives(HashSet<Directive>);

fn reset(mut directives: ResMut<Directives>) {
    directives.clear();
}
