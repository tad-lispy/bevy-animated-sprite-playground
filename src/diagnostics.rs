use bevy::{
    diagnostic::{Diagnostics, FrameTimeDiagnosticsPlugin},
    prelude::*,
};

pub struct DiagnosticsPlugin;

impl Plugin for DiagnosticsPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(FrameTimeDiagnosticsPlugin::default())
            .add_startup_system(setup)
            .add_system(update);
    }
}

#[derive(Component)]
struct FPSCounter;

fn setup(mut commands: Commands, assets_server: Res<AssetServer>) {
    let font = assets_server.load("fonts/terminess-bold-nerd-font-complete-mono.ttf");
    commands
        .spawn_bundle(TextBundle::from_section(
            "Wait for it",
            TextStyle {
                font,
                font_size: 20.0,
                color: Color::PINK,
            },
        ))
        .insert(FPSCounter);
}

fn update(mut counter: Query<&mut Text, With<FPSCounter>>, diagnostics: Res<Diagnostics>) {
    if let Some(fps) = diagnostics
        .get(FrameTimeDiagnosticsPlugin::FPS)
        .and_then(|fps| fps.average())
    {
        counter.single_mut().sections[0].value = format!("{fps:.2} FPS")
    }
}
