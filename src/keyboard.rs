use crate::{
    directives::{Directive, Directives},
    Systems,
};
use bevy::prelude::*;

pub struct KeyboardPlugin;

impl Plugin for KeyboardPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(input.after(Systems::DirectivesReset).label(Systems::Input));
    }
}

fn input(keyboard: Res<Input<KeyCode>>, mut directives: ResMut<Directives>) {
    if keyboard.pressed(KeyCode::Space) {
        directives.insert(Directive::Attack);
    }

    if keyboard.pressed(KeyCode::Up) {
        directives.insert(Directive::Walk);
    }

    if keyboard.pressed(KeyCode::Down) {
        directives.insert(Directive::Reverse);
    }

    if keyboard.pressed(KeyCode::Left) {
        directives.insert(Directive::TurnLeft);
    }

    if keyboard.pressed(KeyCode::Right) {
        directives.insert(Directive::TurnRight);
    }
}
