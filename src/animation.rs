use std::{
    hash::Hash,
    marker::{Send, Sync},
    time::Duration,
};

use benimator::FrameRate;
use bevy::{prelude::*, utils::HashMap};
use enum_iterator::{all, Sequence};

/// A Bevy component holding a Benimator animation, together with it's state and frames
#[derive(Component)]
pub struct Animation {
    pub parameters: benimator::Animation,
    pub state: benimator::State,
    pub frames: Frames,
}

impl Animation {
    pub fn update(&mut self, delta: Duration) {
        self.state.update(&self.parameters, delta);
    }

    pub fn frame(&self) -> Frame {
        self.frames[self.state.frame_index()].clone()
    }
}

/// Frames are handles to images used for animation
pub type Frames = Vec<Frame>;
pub type Frame = Handle<Image>;

impl From<&Frames> for Animation {
    /// Convert frames to an animation
    fn from(frames: &Frames) -> Self {
        Self {
            parameters: benimator::Animation::from_indices(
                0..frames.len(),
                FrameRate::from_fps(15.0),
            )
            .once(),
            state: benimator::State::new(),
            frames: frames.clone(),
        }
    }
}

/// Library is a collection of frame sequences ready to be converted into an animation
#[derive(Deref)]
pub struct Library<Key>(HashMap<Key, Frames>);

impl<Key> Library<Key>
where
    Key: Clone + Sequence + Eq + Hash + Send + Sync + 'static,
    AnimationFramesSpec: From<Key>,
{
    pub fn initialize(mut commands: Commands, assets_server: Res<AssetServer>) {
        let data: HashMap<Key, Frames> = all::<Key>()
            .map(|key| {
                (
                    key.clone(),
                    AnimationFramesSpec::from(key).load(&assets_server),
                )
            })
            .collect();

        let animations = Self::from(data);

        commands.insert_resource(animations);
    }

    pub fn all_frames(&self) -> Frames {
        self.values().fold(Vec::new(), |mut collected, sequence| {
            collected.append(&mut sequence.clone());
            collected
        })
    }
}

impl<Key> From<HashMap<Key, Frames>> for Library<Key> {
    fn from(data: HashMap<Key, Frames>) -> Self {
        Library(data)
    }
}

pub struct AnimationFramesSpec {
    /// Name of the entity, like "zombie" or "player".
    ///
    /// It is used to construct the path.
    pub entity: String,

    /// Name of the activity, like "walk" or "attack".
    ///
    /// It is used to construct the path.
    pub activity: String,

    /// Total number of available frame assets (image files in a directory).
    pub total_frames: usize,

    /// Number of frames in the animation.
    ///
    /// Can be shorter or longer then total frames.
    pub length: usize,

    /// First frame of the animation.
    ///
    /// Zero based. Together with the length, it allows to take a slice of the sequence. If `first_frame + length` is greater than `total_frames` the sequence will wrap, adding the first frame after the last one, then second etc.
    pub first_frame: usize,

    /// Shall frames be played in reverse?
    pub reverse: bool,
}

impl AnimationFramesSpec {
    pub fn load(&self, assets_server: &Res<AssetServer>) -> Frames {
        (0..self.length)
            .map(|index| {
                let index = if self.reverse {
                    -(index as isize)
                } else {
                    index as isize
                };
                let frame_index =
                    (index + self.first_frame as isize).rem_euclid(self.total_frames as isize) + 1;
                debug!("Index {index}, frame: {frame_index}");
                let path = format!(
                    "{entity}/{activity}/{activity}-{frame_index:02}.png",
                    entity = self.entity,
                    activity = self.activity
                );
                debug!("Loading {path}");

                assets_server.load(path.as_str())
            })
            .collect()
    }
}
