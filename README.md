My playground to learn animations in Bevy game engine.

# Credits

"Zombie" 3d model (https://skfb.ly/6zG9q) by DJMaesen is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).
